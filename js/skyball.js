/**
 * Sets a localStorage setting.
 * @param name
 * @param value
 */
function setSetting(name, value) {
    var testObject = { 'value': value };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * Gets a localStorage setting.
 * @param name
 * @returns {*}
 */
function getSetting(name) {
    var retrievedObject = localStorage.getItem(name);
    if (!retrievedObject) {
        return false;
    }

    var jsonObject = JSON.parse(retrievedObject);
    return jsonObject.value;
}

/**
 * Google button hack.
 */
var checkExist = setInterval(function() {
    if ($('.abcRioButtonContentWrapper').length) {
        $("span:contains('Sign in with Google')").text('Akadémiás bejelentkezés');

        clearInterval(checkExist);
    }
}, 100);

/**
 * Google login success handler.
 * @param googleUser
 */
function loginSuccess(googleUser) {
    var gauth = gapi.auth2.getAuthInstance(),
        username = googleUser.getBasicProfile().getName(),
        mail = googleUser.getBasicProfile().getEmail(),
        token = googleUser.getAuthResponse().id_token;

    setSetting('skyball-username', username);
    setSetting('skyball-mail', mail);
    setSetting('skyball-token', token);

    console.log(googleUser);
    console.log(gauth);
    console.log(username);
    console.log(mail);
    console.log(token);

    $.ajax({
        type: "POST",
        url: 'skyball-stats/web/auth',
        data: {
            token: token,
            mail: mail
        },
        success: function(data) {
            console.log(data);
        }
    });
}

/**
 * Google login error handler.
 * @param error
 */
function loginFail(error) {
    console.log('Login error', error);
}

/**
 * Get teams.
 */
function getTeams() {
    $.get('skyball-stats/web/team', function(teams) {
        var i,
            j,
            team,
            string,
            players = [],
            weight,
            player,
            playerString;

        if (teams) {
            for (i in teams) {
                team = teams[i];
                playerString = '';

                for (j in team['players']) {
                    weight = team['players'][j]['weight'];
                    players[weight] = team['players'][j];
                }
                for (j in players) {
                    player = players[j];
                    playerString += '<td class="player' + j + '">' +
                        player['name'] + ' (' + player['number'] + ')' +
                        '</td>';
                }

                string = '<tr>' +
                    '<td class="name">' + team['name'] + '</td>' +
                    playerString +
                    '<td class="point">' + 0 + '</td>' +
                    '</tr>';

                $('#team-body').append(string);
            }

            var table = $("#team-table").show().dataTable({
                "paging":   false,
                "info":     false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json"
                },
                "columns": [
                    {"name": "first", "orderable": true},
                    {"name": "player1", "orderable": false},
                    {"name": "player2", "orderable": false},
                    {"name": "player3", "orderable": false},
                    {"name": "point", "orderable": true}
                ]
            });
        }
        else {
            $('#no-teams').show();
        }
    });
}

getTeams();