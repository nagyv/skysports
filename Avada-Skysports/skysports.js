'use strict';

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          window.oRequestAnimationFrame      ||
          window.msRequestAnimationFrame     ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();

(function(win, d, wp_vars) {

  function getScrollPercent() {
    let h = document.documentElement,
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';
    return b[st] / ((b[sh]) - h.clientHeight);
  }

  function getPath(path) {
    if(wp_vars) {
        path = wp_vars.templateUrl + '/' + path;
    }
    return path;
  }

  var Cloud = function(initialX, initialY, endX, sizeX, sizeY, file, canvas) {
    this.img= new Image();   // Create new img element
    this.img.src = file; // Set source path

    this.initialX = initialX;
    this.initialY = initialY;
    this.endX = endX;
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    this.ctx = canvas;
  }
  Cloud.prototype.update = function(movePercent) {
    if(!this.ctx) return;

    this.ctx.drawImage(this.img, this.initialX + (this.endX-this.initialX) * movePercent, this.initialY, this.sizeY, this.sizeX);
  }

  var Sun = function(initialX, initialY, endX, sizeX, sizeY, file, canvas) {
      this.img = new Image();   // Create new img element
      this.img.src = getPath('img/nap.svg'); // Set source path

      this.initialX = initialX;
    this.initialY = initialY;
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    this.endX = endX;
    this.ctx = canvas;
  }
  Sun.prototype.update = function(movePercent) {
    if(!this.ctx) return;

    let sunPos = this._circlePos(movePercent);
    ctx.drawImage(this.img, sunPos[0], sunPos[1], this.sizeY + 0.3 * this.sizeY * movePercent, this.sizeY + 0.3 * this.sizeY * movePercent);
  }
  Sun.prototype._circlePos = function(movePercent) {
    let y = Math.sqrt(movePercent);
    return [this.initialX + movePercent * (this.endX-this.initialX), this.initialY - y * this.initialY]
  }

  var Sky = function(canvas) {
    this.canvas = canvas;
    this.ctx = canvas.getContext('2d');
    this.clouds = [];
  }
  Sky.prototype.drawBg = function drawBg() {
      // Create gradients
      var lingrad = this.ctx.createLinearGradient(0,0,0,window.innerHeight);
      lingrad.addColorStop(0, '#a8d5f5');
      lingrad.addColorStop(0.8, '#ddeefa');
      lingrad.addColorStop(0.8, '#26C000');
      lingrad.addColorStop(1, '#a2ffa2');

      // assign gradients to fill and stroke styles
      this.ctx.fillStyle = lingrad;

      // draw shapes
      this.ctx.fillRect(0, 0, window.innerWidth, window.innerHeight);
  }
  Sky.prototype.update = function(movePercent) {
    this.drawBg();
    this.clouds.forEach(function(cloud) {
        cloud.update(movePercent);
    });
  }
  Sky.prototype.addSun = function(initialX, initialY, endX, sizeX, sizeY) {
    var sun = new Sun(initialX, initialY, endX, sizeX, sizeY, getPath('img/nap.svg'), this.ctx)
    this.clouds.push(sun);
    return sun;
  }
  Sky.prototype.addCloud = function(initialX, initialY, endX, sizeX, sizeY, file) {
    var cloud = new Cloud(initialX, initialY, endX, sizeX, sizeY, file, this.ctx);
    this.clouds.push(cloud);
    return cloud;
  }
  Sky.prototype.createSky = function(sunHeight, sunBase, canvasWidth) {
    this.sunHeight = sunHeight;
    this.sunBase = sunBase;
    this.canvasWidth = canvasWidth;
      this.addSun(-1*sunHeight/1.5, sunBase, canvasWidth - sunHeight, sunHeight, sunHeight);
      this.addCloud(100, 160, canvasWidth - 160, 40, 400, getPath('img/felho3.svg'));
      this.addCloud(230, 80, canvasWidth - 160, 80, 400, getPath('img/felho3.svg'));
      this.addCloud(90, 70, canvasWidth - 100, 60, 300, getPath('img/felho2.svg'));
      this.addCloud(0, 12, canvasWidth - 80, 96, 270, getPath('img/felho1.svg'));
  }
  Sky.prototype.onResize = function() {
    this.canvasWidth = this.canvas.height
    this.sunBase = this.canvasWidth * 0.8 - (this.sunHeight*1.5);
  }

  var $ = d.querySelector.bind(d);

  var canvas = $('#skysportsBg');
  if (!canvas.getContext){
    return;
  }
  canvas.width  = window.innerWidth;
  canvas.height = window.innerHeight;

  var sunHeight = 100;

  var ticking = false;
  var ctx = canvas.getContext('2d');

  var sky = new Sky(canvas);
  sky.createSky(sunHeight, canvas.height * 0.8 - (sunHeight*1.5), canvas.width);

  function onResize () {
    canvas.width  = window.innerWidth;
    canvas.height = window.innerHeight;
    sky.onResize();
    updateElements();
  }

  function onScroll (evt) {
    if(!ticking) {
      ticking = true;
      requestAnimFrame(updateElements);
    }
  }

  function updateElements () {
    sky.update(getScrollPercent())
    ticking = false;
  }

  win.addEventListener('load', onResize, false);
  win.addEventListener('resize', onResize, false);
  win.addEventListener('scroll', onScroll, false);

})(window, document, wp_vars);