<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'avada-parent-stylesheet', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );

function skysports_enqueue_script() {
	wp_enqueue_script( 'skysports-js', get_stylesheet_directory_uri() . '/skysports.js', array(), false, true );
	$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
	wp_localize_script( 'skysports-js', 'wp_vars', $translation_array );
}
add_action( 'wp_enqueue_scripts', 'skysports_enqueue_script' );

function avada_add_skysportsBg() {
 echo '<canvas id="skysportsBg"></canvas>';
}
add_action( 'avada_after_main_container', 'avada_add_skysportsBg' );
