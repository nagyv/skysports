<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Admin controller.
 *
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/composer", name="composer")
     * @param Request $request
     * @return JsonResponse
     */
    public function composerAction(Request $request)
    {
        chdir($this->get('kernel')->getRootDir() . '/../');
        exec('composer update 2>&1', $output);

        return new JsonResponse($output);
    }

    /**
     * @Route("/dbupdate", name="dbupdate")
     * @param Request $request
     * @return JsonResponse
     */
    public function dbUpdateAction(Request $request)
    {
        chdir($this->get('kernel')->getRootDir() . '/../');
        exec('php bin/console doctrine:schema:update --force 2>&1', $output);

        return new JsonResponse($output);
    }

    /**
     * @Route("/dbinit", name="dbinit")
     * @param Request $request
     * @return JsonResponse
     */
    public function dbInitAction(Request $request)
    {
        chdir($this->get('kernel')->getRootDir() . '/../');
        exec('php bin/console doctrine:fixtures:load --append 2>&1', $output);

        return new JsonResponse($output);
    }
}
