<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Repository\NewsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\News;
use AppBundle\Form\NewsType;

/**
 * News controller.
 *
 * @Route("/news")
 */
class NewsController extends Controller
{
    /**
     * Lists all News entities.
     *
     * @Route("/", name="news")
     * @Method("GET")
     * @Template()
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser()) {
            /** @var NewsRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:News');
            $news = $repo->getAllNewsQuery();

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $news,
                $request->query->getInt('p', 1),
                20
            );

            return array(
                'news' => $pagination,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a new News entity.
     *
     * @Route("/new/", name="news_new")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(Request $request)
    {
        if ($this->getUser() && $this->getUser()->isAuthor()) {
            $news = new News();
            $form = $this->createForm('AppBundle\Form\NewsType', $news);
            $form->handleRequest($request);
            $date = new \DateTime();
            $news->setDate($date);
            $news->setUpdated($date);
            $news->setAuthorEmail($this->getUser()->getEmail());

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($news);
                $em->flush();

                return $this->redirectToRoute('news');
            }

            return array(
                'news' => $news,
                'form' => $form->createView(),
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Finds and displays a News entity.
     *
     * @Route("/{id}/", name="news_show")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showAction(Request $request, $id)
    {
        if ($this->getUser()) {
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:News');
            /** @var News $news */
            $news = $repo->getNews($id);

            if (!$news) {
                return $this->redirectToRoute('news');
            }

            $deleteForm = $this->createDeleteForm($news);

            $comment = new Comment();
            $form = $this->createForm('AppBundle\Form\CommentType', $comment);
            $form->handleRequest($request);
            $comment->setNews($news);
            $comment->setAuthor($this->getUser()->getFullname());
            $date = new \DateTime();
            $comment->setDate($date);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();

                return $this->redirectToRoute('news_show', array('id' => $news->getId()));
            }

            return array(
                'news' => $news,
                'comment' => $comment,
                'form' => $form->createView(),
                'delete_form' => $deleteForm->createView(),
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Displays a form to edit an existing News entity.
     *
     * @Route("/{id}/edit/", name="news_edit")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param News $news
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, News $news)
    {
        if ($this->getUser()) {
            if ($this->getUser()->isAuthor() && $news->getAuthorEmail() == $this->getUser()->getEmail()) {
                $deleteForm = $this->createDeleteForm($news);
                $editForm = $this->createForm('AppBundle\Form\NewsType', $news);
                $editForm->handleRequest($request);
                $date = new \DateTime();
                $news->setUpdated($date);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($news);
                    $em->flush();

                    return $this->redirectToRoute('news');
                }

                return array(
                    'news' => $news,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                );
            }
            else {
                return $this->redirectToRoute('news');
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Deletes a News entity.
     *
     * @Route("/{id}/", name="news_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param News $news
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, News $news)
    {
        if ($this->getUser()) {
            if ($this->getUser()->isAuthor() && $news->getAuthorEmail() == $this->getUser()->getEmail()) {
                $form = $this->createDeleteForm($news);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($news);
                    $em->flush();
                }

                return $this->redirectToRoute('news');
            }
            else {
                return $this->redirectToRoute('news');
            }
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a form to delete a News entity.
     *
     * @param News $news The News entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(News $news)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('news_delete', array('id' => $news->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
