<?php

namespace AppBundle\Controller;

use AppBundle\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Team controller.
 *
 * @Route("/")
 */
class TeamController extends Controller
{
    /**
     * Lists all Team entities.
     *
     * @Route("/teams/", name="team", options={"expose" = true})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            /** @var TeamRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Team');
            $teams = $repo->getTeams();

            return array(
                'teams' => $teams,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Lists all Team entities in json.
     *
     * @Route("/api/teams/", name="team_api_all")
     * @Method("GET")
     */
    public function getAllAction()
    {
        if ($this->getUser()) {
            /** @var TeamRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Team');
            $teams = $repo->getTeams();

            return new JsonResponse($teams);
        }
        else {
            return new JsonResponse('nope');
        }
    }

    /**
     * Finds and displays a Team entity.
     *
     * @Route("/teams/{id}/", name="team_show")
     * @Method("GET")
     * @Template
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        if ($this->getUser()) {
            /** @var TeamRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Team');
            $team = $repo->getTeam($id);

            return array(
                'team' => $team,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Finds and displays a Team entity.
     *
     * @Route("/api/teams/{id}/", name="team_api_one")
     * @Method("GET")
     * @Template
     * @param $id
     * @return array
     */
    public function getOneAction($id)
    {
        if ($this->getUser()) {
        /** @var TeamRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Team');
        $team = $repo->getTeam($id);

        return new JsonResponse($team);
        }
        else {
            return new JsonResponse('nope');
        }
    }
}
