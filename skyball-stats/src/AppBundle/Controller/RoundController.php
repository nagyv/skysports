<?php

namespace AppBundle\Controller;

use AppBundle\Repository\RoundRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Round;
use AppBundle\Form\RoundType;

/**
 * Round controller.
 *
 * @Route("/round")
 */
class RoundController extends Controller
{
    /**
     * Lists all Round entities.
     *
     * @Route("/", name="round")
     * @Method("GET")
     * @Template()
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser()) {
            /** @var RoundRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Round');
            $rounds = $repo->getAllRoundsQuery();
            $currentRound = 1;
            $currentRoundObject = $this->getDoctrine()->getManager()->getRepository('AppBundle:Settings')->findOneBy(array(
                'name' => 'currentRound',
            ));
            if ($currentRoundObject) {
                $currentRound = (int)$currentRoundObject->getValue();
            }

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $rounds,
                $request->query->getInt('r', $currentRound),
                1,
                array(
                    'pageParameterName' => 'r',
                )
            );

            return array(
                'rounds' => $pagination,
            );
        }
        else {
            return $this->redirectToRoute('index');
        }
    }

    /**
     * Creates a new Round entity.
     *
     * @Route("/new", name="round_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $round = new Round();
        $form = $this->createForm('AppBundle\Form\RoundType', $round);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($round);
            $em->flush();

            return $this->redirectToRoute('round_show', array('id' => $round->getId()));
        }

        return $this->render('round/new.html.twig', array(
            'round' => $round,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Round entity.
     *
     * @Route("/{id}", name="round_show")
     * @Method("GET")
     */
    public function showAction(Round $round)
    {
        $deleteForm = $this->createDeleteForm($round);

        return $this->render('round/show.html.twig', array(
            'round' => $round,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Round entity.
     *
     * @Route("/{id}/edit", name="round_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Round $round)
    {
        $deleteForm = $this->createDeleteForm($round);
        $editForm = $this->createForm('AppBundle\Form\RoundType', $round);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($round);
            $em->flush();

            return $this->redirectToRoute('round_edit', array('id' => $round->getId()));
        }

        return $this->render('round/edit.html.twig', array(
            'round' => $round,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Round entity.
     *
     * @Route("/{id}", name="round_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Round $round)
    {
        $form = $this->createDeleteForm($round);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($round);
            $em->flush();
        }

        return $this->redirectToRoute('round_index');
    }

    /**
     * Creates a form to delete a Round entity.
     *
     * @param Round $round The Round entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Round $round)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('round_delete', array('id' => $round->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
