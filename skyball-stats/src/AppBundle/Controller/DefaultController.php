<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index_redirect", options={"expose" = true})
     * @Route("/homepage/", name="index", options={"expose" = true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Template()
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('team');
        }
        return array();
    }

    /**
     * @Route("/auth/", name="auth", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function authAction(Request $request) {
        $token = $request->request->get('token');
        $oauthService = $this->get('skyball_oauth');

        return $oauthService->auth($token);
    }

    /**
     * @Route("/check-auth/", name="check_auth", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkAuthAction(Request $request) {
        return $this->getUser() ? new JsonResponse(true) : new JsonResponse(false);
    }

    /**
     * Log the user out.
     * @Route("/logout/", name="logout", options={"expose" = true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logoutAction(Request $request) {
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        return $this->redirectToRoute('index');
    }
}
