<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class NewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        global $kernel;
        $container = $kernel->getContainer();
        $user = $container->get('security.token_storage')->getToken()->getUser();
        $username = $user->getFullName();

        $builder
            ->add('title', null, array(
                'label' => 'news.title'
            ))
            ->add('body',  CKEditorType::class, array(
                'config' => array(
                    'filebrowser_image_browse_url' => array(
                        'route'            => 'elfinder',
                        'route_parameters' => array('instance' => 'ckeditor'),
                    ),
                ),
                'label' => 'news.body'
            ))
            ->add('author', null, array(
                'label' => 'news.author',
                'data' => $username,
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\News'
        ));
    }
}
