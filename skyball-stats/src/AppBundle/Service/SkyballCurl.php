<?php

namespace AppBundle\Service;

use AppBundle\Entity\GoogleUser;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SkyballCurl extends Controller
{
    protected $curl;
    protected $key;
    protected $em;
    protected $ts;
    protected $ed;

    /**
     * Constructor
     * @param EntityManager $em
     * @param TokenStorage $ts
     * @param TraceableEventDispatcher $ed
     */
    public function __construct(EntityManager $em, TokenStorage $ts, TraceableEventDispatcher $ed)
    {
        $this->key = 'ujrakezdok@googlegroups.com';
        $this->curl = curl_init();
        $this->em = $em;
        $this->ts = $ts;
        $this->ed = $ed;
        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    }

    /**
     * Get url.
     * @param $url
     * @return mixed
     */
    public function get($url) {
        curl_setopt($this->curl, CURLOPT_URL, $url);

        $res = curl_exec($this->curl);
        $result = json_decode($res, true);

        return $result;
    }

    /**
     * Check if user is in our group
     * @param $mail
     * @return mixed
     */
    public function isUserInGroup($mail) {
        $user = $this->em
            ->getRepository('AppBundle:GoogleUser')
            ->createQueryBuilder('u')
            ->select('u')
            ->where('u.email = :mail')
            ->setParameter('mail', $mail)
            ->getQuery()
            ->getResult();

        if ($user) {
            /** @var GoogleUser $user */
            $user = $user[0];

            $token = new UsernamePasswordToken($user, $user->getPassword(), "public", $user->getRoles());
            $this->ts->setToken($token);
            $event = new InteractiveLoginEvent(new Request(), $token);
            $this->ed->dispatch("security.interactive_login", $event);

            $user = [
                'name' => $user->getUsername(),
                'mail' => $user->getEmail(),
                'rule' => $user->getRule(),
                'ruleString' => $this->getRuleString($user->getRule()),
            ];
        }
        else {
            $user = false;
        }

        return $user;
    }

    /**
     * Get ruleString
     *
     * @param $rule
     * @return string
     */
    protected function getRuleString($rule)
    {
        $string = 'Felhasználó';

        switch ($rule) {
            case 1:
                $string = 'Sajtós';
                break;

            case 2:
                $string = 'Admin';
                break;

            case 3:
                $string = 'Admin';
                break;
        }

        return $string;
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        curl_close($this->curl);
    }

    /**
     * Destructor
     */
    public function close()
    {
        curl_close($this->curl);
    }
}
