<?php
namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SkyballOauth extends Controller
{
    protected $curl;

    /**
     * @param \AppBundle\Service\SkyballCurl $curl
     */
    public function __construct(SkyballCurl $curl) {
        $this->curl = $curl;
    }

    public function auth($token)
    {
        $result = $this->getTokenResult($token);

        // if we can authenticate the user via GOAuth
        if ($result && isset($result['email'])) {
            $user = $this->curl->isUserInGroup($result['email']);
            return new JsonResponse($user, 200);
        }

        return new JsonResponse(false, 404);
    }

    public function checkAuth($token) {
        $result = $this->getTokenResult($token);

        // if we can authenticate the user via GOAuth
        if ($result && isset($result['email']) || $this->getUser()) {
            return new JsonResponse(true, 200);
        }

        return new JsonResponse(false, 404);
    }

    /**
     * Get token result.
     *
     * @param $token
     * @return mixed
     */
    protected function getTokenResult($token) {
        $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $token;

        return $this->curl->get($url);
    }
}