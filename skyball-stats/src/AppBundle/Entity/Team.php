<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="team")
     */
    protected $players;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamA")
     */
    protected $gamesA;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="teamB")
     */
    protected $gamesB;

    /**
     * @ORM\OneToMany(targetEntity="RoundResult", mappedBy="team")
     */
    protected $roundResults;

    /**
     * @var int
     *
     * @ORM\Column(name="point", type="integer", nullable=true)
     */
    protected $point;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return Team
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \AppBundle\Entity\Player $player
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add gamesA
     *
     * @param \AppBundle\Entity\Game $gamesA
     *
     * @return Team
     */
    public function addGamesA(\AppBundle\Entity\Game $gamesA)
    {
        $this->gamesA[] = $gamesA;

        return $this;
    }

    /**
     * Remove gamesA
     *
     * @param \AppBundle\Entity\Game $gamesA
     */
    public function removeGamesA(\AppBundle\Entity\Game $gamesA)
    {
        $this->gamesA->removeElement($gamesA);
    }

    /**
     * Get gamesA
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesA()
    {
        return $this->gamesA;
    }

    /**
     * Add gamesB
     *
     * @param \AppBundle\Entity\Game $gamesB
     *
     * @return Team
     */
    public function addGamesB(\AppBundle\Entity\Game $gamesB)
    {
        $this->gamesB[] = $gamesB;

        return $this;
    }

    /**
     * Remove gamesB
     *
     * @param \AppBundle\Entity\Game $gamesB
     */
    public function removeGamesB(\AppBundle\Entity\Game $gamesB)
    {
        $this->gamesB->removeElement($gamesB);
    }

    /**
     * Get gamesB
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesB()
    {
        return $this->gamesB;
    }

    /**
     * Set point
     *
     * @param integer $point
     *
     * @return Team
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Add roundResult
     *
     * @param \AppBundle\Entity\RoundResult $roundResult
     *
     * @return Team
     */
    public function addRoundResult(\AppBundle\Entity\RoundResult $roundResult)
    {
        $this->roundResults[] = $roundResult;

        return $this;
    }

    /**
     * Remove roundResult
     *
     * @param \AppBundle\Entity\RoundResult $roundResult
     */
    public function removeRoundResult(\AppBundle\Entity\RoundResult $roundResult)
    {
        $this->roundResults->removeElement($roundResult);
    }

    /**
     * Get roundResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundResults()
    {
        return $this->roundResults;
    }

    public function __toString() {
        return $this->name;
    }
}
