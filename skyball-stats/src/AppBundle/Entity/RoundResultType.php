<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoundResultType
 *
 * @ORM\Table(name="round_result_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoundResultTypeRepository")
 */
class RoundResultType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="computerName", type="string", length=100, unique=true)
     */
    protected $computerName;

    /**
     * @var int
     *
     * @ORM\Column(name="point", type="smallint")
     */
    protected $point;

    /**
     * @ORM\OneToMany(targetEntity="RoundResult", mappedBy="roundResultType")
     */
    protected $roundResults;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RoundResultType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set computerName
     *
     * @param string $computerName
     *
     * @return RoundResultType
     */
    public function setComputerName($computerName)
    {
        $this->computerName = $computerName;

        return $this;
    }

    /**
     * Get computerName
     *
     * @return string
     */
    public function getComputerName()
    {
        return $this->computerName;
    }

    /**
     * Set point
     *
     * @param integer $point
     *
     * @return RoundResultType
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return int
     */
    public function getPoint()
    {
        return $this->point;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roundResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add roundResult
     *
     * @param \AppBundle\Entity\RoundResult $roundResult
     *
     * @return RoundResultType
     */
    public function addRoundResult(\AppBundle\Entity\RoundResult $roundResult)
    {
        $this->roundResults[] = $roundResult;

        return $this;
    }

    /**
     * Remove roundResult
     *
     * @param \AppBundle\Entity\RoundResult $roundResult
     */
    public function removeRoundResult(\AppBundle\Entity\RoundResult $roundResult)
    {
        $this->roundResults->removeElement($roundResult);
    }

    /**
     * Get roundResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundResults()
    {
        return $this->roundResults;
    }

    public function __toString() {
        return $this->name;
    }
}
