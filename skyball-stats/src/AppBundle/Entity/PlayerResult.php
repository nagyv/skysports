<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlayerResult
 *
 * @ORM\Table(name="player_result")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlayerResultRepository")
 */
class PlayerResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="smallint")
     */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="playerResults")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;

    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="playerResults")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;

    /**
     * @ORM\ManyToOne(targetEntity="KerchiefType", inversedBy="playerResults")
     * @ORM\JoinColumn(name="kerchieftype_id", referencedColumnName="id")
     */
    protected $kerchiefType;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return PlayerResult
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return PlayerResult
     */
    public function setPlayer(\AppBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \AppBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return PlayerResult
     */
    public function setGame(\AppBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set kerchief
     *
     * @param \AppBundle\Entity\KerchiefType $kerchief
     *
     * @return PlayerResult
     */
    public function setKerchief(\AppBundle\Entity\KerchiefType $kerchief = null)
    {
        $this->kerchief = $kerchief;

        return $this;
    }

    /**
     * Get kerchief
     *
     * @return \AppBundle\Entity\KerchiefType
     */
    public function getKerchief()
    {
        return $this->kerchiefType;
    }

    /**
     * Set kerchiefType
     *
     * @param \AppBundle\Entity\KerchiefType $kerchiefType
     *
     * @return PlayerResult
     */
    public function setKerchiefType(\AppBundle\Entity\KerchiefType $kerchiefType = null)
    {
        $this->kerchiefType = $kerchiefType;

        return $this;
    }

    /**
     * Get kerchiefType
     *
     * @return \AppBundle\Entity\KerchiefType
     */
    public function getKerchiefType()
    {
        return $this->kerchiefType;
    }

    public function __toString() {
        return $this->game . ' - ' . $this->player . ' - ' . $this->kerchiefType . ' - ' . $this->value;
    }
}
