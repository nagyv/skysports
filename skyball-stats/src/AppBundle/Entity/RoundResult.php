<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoundResult
 *
 * @ORM\Table(name="round_result")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoundResultRepository")
 */
class RoundResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="RoundResultType", inversedBy="roundResults")
     * @ORM\JoinColumn(name="roundresulttype_id", referencedColumnName="id")
     */
    protected $roundResultType;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="roundResults")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roundResultType
     *
     * @param \AppBundle\Entity\RoundResultType $roundResultType
     *
     * @return RoundResult
     */
    public function setRoundResultType(\AppBundle\Entity\RoundResultType $roundResultType = null)
    {
        $this->roundResultType = $roundResultType;

        return $this;
    }

    /**
     * Get roundResultType
     *
     * @return \AppBundle\Entity\RoundResultType
     */
    public function getRoundResultType()
    {
        return $this->roundResultType;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     *
     * @return RoundResult
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    public function __toString() {
        return $this->team . ' - ' . $this->roundResultType;
    }
}
