<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * GoogleUser
 *
 * @ORM\Table(name="google_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GoogleUserRepository")
 */
class GoogleUser extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=255)
     */
    protected $fullName;

    /**
     * @var int
     * 0 - mezei user, 1 - sajtós, 2 - admin, 3 - én
     *
     * @ORM\Column(name="rule", type="smallint", options={"default" = 0})
     */
    protected $rule;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set rule
     *
     * @param integer $rule
     *
     * @return GoogleUser
     */
    public function setRule($rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return integer
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return GoogleUser
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    public function isSu() {
        return $this->rule >= 3;
    }
    public function isAdmin() {
        return $this->rule >= 2;
    }
    public function isAuthor() {
        return $this->rule >= 1;
    }

    public function __toString() {
        return $this->fullName;
    }
}
