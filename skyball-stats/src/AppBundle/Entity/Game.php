<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesA")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $teamA;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="gamesB")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $teamB;

    /**
     * @var int
     *
     * @ORM\Column(name="pointa", type="smallint", nullable=true)
     */
    protected $pointA;

    /**
     * @var int
     *
     * @ORM\Column(name="pointb", type="smallint", nullable=true)
     */
    protected $pointB;

    /**
     * @ORM\OneToMany(targetEntity="PlayerResult", mappedBy="game")
     */
    protected $playerResults;

    /**
     * @ORM\ManyToOne(targetEntity="GameType", inversedBy="games")
     * @ORM\JoinColumn(name="gametype_id", referencedColumnName="id")
     */
    protected $gameType;

    /**
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="games")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    protected $round;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playerResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set pointA
     *
     * @param integer $pointA
     *
     * @return Game
     */
    public function setPointA($pointA)
    {
        $this->pointA = $pointA;

        return $this;
    }

    /**
     * Get pointA
     *
     * @return integer
     */
    public function getPointA()
    {
        return $this->pointA;
    }

    /**
     * Set pointB
     *
     * @param integer $pointB
     *
     * @return Game
     */
    public function setPointB($pointB)
    {
        $this->pointB = $pointB;

        return $this;
    }

    /**
     * Get pointB
     *
     * @return integer
     */
    public function getPointB()
    {
        return $this->pointB;
    }

    /**
     * Set teamA
     *
     * @param \AppBundle\Entity\Team $teamA
     *
     * @return Game
     */
    public function setTeamA(\AppBundle\Entity\Team $teamA = null)
    {
        $this->teamA = $teamA;

        return $this;
    }

    /**
     * Get teamA
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeamA()
    {
        return $this->teamA;
    }

    /**
     * Set teamB
     *
     * @param \AppBundle\Entity\Team $teamB
     *
     * @return Game
     */
    public function setTeamB(\AppBundle\Entity\Team $teamB = null)
    {
        $this->teamB = $teamB;

        return $this;
    }

    /**
     * Get teamB
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeamB()
    {
        return $this->teamB;
    }

    /**
     * Add playerResult
     *
     * @param \AppBundle\Entity\PlayerResult $playerResult
     *
     * @return Game
     */
    public function addPlayerResult(\AppBundle\Entity\PlayerResult $playerResult)
    {
        $this->playerResults[] = $playerResult;

        return $this;
    }

    /**
     * Remove playerResult
     *
     * @param \AppBundle\Entity\PlayerResult $playerResult
     */
    public function removePlayerResult(\AppBundle\Entity\PlayerResult $playerResult)
    {
        $this->playerResults->removeElement($playerResult);
    }

    /**
     * Get playerResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerResults()
    {
        return $this->playerResults;
    }

    /**
     * Set gameType
     *
     * @param \AppBundle\Entity\GameType $gameType
     *
     * @return Game
     */
    public function setGameType(\AppBundle\Entity\GameType $gameType = null)
    {
        $this->gameType = $gameType;

        return $this;
    }

    /**
     * Get gameType
     *
     * @return \AppBundle\Entity\GameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * Set round
     *
     * @param \AppBundle\Entity\Round $round
     *
     * @return Game
     */
    public function setRound(\AppBundle\Entity\Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return \AppBundle\Entity\Round
     */
    public function getRound()
    {
        return $this->round;
    }

    public function __toString() {
        $teamA = $this->teamA ? $this->teamA : '?';
        $teamB = $this->teamB ? $this->teamB : '?';
        return $teamA . ' vs ' . $teamB;
    }
}
