/**
 * Set a localStorage setting.
 * @param name
 * @param value
 */
function setSetting(name, value) {
    var testObject = { 'value': value };
    localStorage.setItem(name, JSON.stringify(testObject));
}

/**
 * Get a localStorage setting.
 * @param name
 */
function getSetting(name) {
    var retrievedObject = localStorage.getItem(name);
    if (!retrievedObject) {
        return false;
    }

    var jsonObject = JSON.parse(retrievedObject);
    return jsonObject.value;
}

/**
 * Remove a localStorage setting.
 * @param name
 */
function delSetting(name) {
    localStorage.removeItem(name);
}

/**
 * Set a cookie value.
 * @param name
 * @param value
 * @param days
 */
function setCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

/**
 * Delete a cookie value.
 * @param name
 */
function delCookie(name) {
    setCookie(name, "", -1);
}

/**
 * Google button hack.
 */
var checkExist = setInterval(function() {
    if ($('.abcRioButtonContentWrapper').length) {
        $("span:contains('Sign in with Google')").text('Akadémiás bejelentkezés');

        clearInterval(checkExist);
    }
}, 100);

/**
 * Check if the user is logged in.
 * @returns {*}
 */
function checkLoginAjax() {
    var token = getSetting('skyball-token');

    if (token) {
        return $.ajax({
            type: "POST",
            async: false,
            url: Routing.generate('check_auth'),
            data: {
                token: token
            }
        }).responseJSON;
    }
    else {
        return false;
    }
}

/**
 * Google login success handler.
 * @param googleUser
 */
function loginSuccess(googleUser) {
    var gauth = gapi.auth2.getAuthInstance(),
        username = googleUser.getBasicProfile().getName(),
        mail = googleUser.getBasicProfile().getEmail(),
        token = googleUser.getAuthResponse().id_token;

    $.ajax({
        type: "POST",
        url: Routing.generate('auth'),
        data: {
            token: token,
            mail: mail
        },
        success: function(data) {
            if (data) {
                setSetting('skyball-username', username);
                setSetting('skyball-mail', mail);
                setSetting('skyball-token', token);

                var path = getSetting('skyball-path') ? getSetting('skyball-path') : Routing.generate('news');
                window.location.replace(path);
            }
            else {
                //logout();
            }
        }
    });
}

/**
 * Google login error handler.
 * @param error
 */
function loginFail(error) {
    console.log('Login error', error);
}

/**
 * Logout from google.
 */
function logout() {
    delSetting('skyball-username');
    delSetting('skyball-mail');
    delSetting('skyball-token');
    /*
    gapi.auth2.getAuthInstance().currentUser.get().disconnect();
    gapi.auth2.getAuthInstance().disconnect();
    gapi.auth2.getAuthInstance().signOut();
    delCookie('APSID');
    delCookie('CONSENT');
    delCookie('HSID');
    delCookie('NID');
    delCookie('SAPISID');
    delCookie('SID');
    delCookie('SSID');
    window.open('https://mail.google.com/mail/u/0/?logout&hl=en');
    */
    window.location.replace(Routing.generate('logout'));
}

/**
 * Check if the user is logged in.
 */
function checkLogin() {
    if (checkLoginAjax()) {
        if (window.location.href.indexOf('/homepage/') > -1) {
            var path = getSetting('skyball-path') ? getSetting('skyball-path') : Routing.generate('news');
            window.location.replace(path);
        }
        else {
            $('#header-loggedin').show();
            $('#header-logout').show();
        }
    }
    else {
        if (window.location.href.indexOf('/homepage/') === -1) {
            window.location.replace(Routing.generate('index'));
        }
        else {
            $('#header-signin-button').show();
        }
    }
}

/**
 * Activate the current main menu item.
 */
function activeMenu() {
    var currentHref = window.location.href,
        activeMenu = 'logo',
        menus = ['teams', 'news', 'results', 'players'],
        i,
        menu;

    for (i in menus) {
        menu = menus[i];
        if (currentHref.indexOf('/' + menu + '/') > -1) {
            activeMenu = 'menu-' + menu;
        }
    }

    $(".nav").find(".active").removeClass("active");
    $('#' + activeMenu).addClass('active');
}

function savePath() {
    var path = window.location.pathname;
    if (path.indexOf('/homepage/') > -1) {
        path = path.replace('homepage/', '');
    }
    setSetting('skyball-path', path);
}

checkLogin();
activeMenu();
savePath();