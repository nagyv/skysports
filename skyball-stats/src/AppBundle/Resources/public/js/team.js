var table = $("#team-table").show().dataTable({
    "paging":   false,
    "info":     false,
    "language": {
        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Hungarian.json"
    },
    "columns": [
        {"name": "first", "orderable": true},
        {"name": "player1", "orderable": false},
        {"name": "player2", "orderable": false},
        {"name": "player3", "orderable": false},
        {"name": "point", "orderable": true}
    ],
    "order": [
        [4, "desc"],
        [0, "asc"]
    ]
});