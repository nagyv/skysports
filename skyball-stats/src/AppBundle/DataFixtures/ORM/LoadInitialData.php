<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Game;
use AppBundle\Entity\GameType;
use AppBundle\Entity\GoogleUser;
use AppBundle\Entity\Kerchief;
use AppBundle\Entity\KerchiefType;
use AppBundle\Entity\Round;
use AppBundle\Entity\RoundResultType;
use AppBundle\Entity\Settings;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Team;
use AppBundle\Entity\Player;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $em)
    {
        $data = $this->getData();
        $players = $data['players'];
        $teams = $data['teams'];
        $users = $data['users'];
        $gameTypes = $data['gameTypes'];
        $kerchiefTypes = $data['kerchiefTypes'];
        $roundResultTypes = $data['roundResultTypes'];
        $playerNum = $teamNum = $userNum = $gameTypeNum = $kerchiefTypeNum = $roundResultTypeNum = $roundNum = $gameNum =
        $playerNumU = $teamNumU = $userNumU = $gameTypeNumU = $kerchiefTypeNumU = $roundResultTypeNumU = $roundNumU = 0;

        foreach ($players as $player) {
            $p = $em->getRepository('AppBundle:Player')->findOneBy(array('number' => $player['number']));
            if ($p) {
                $playerNumU++;
            }
            else {
                $p = new Player();
                $playerNum++;
            }

            $p->setName($player['name']);
            $p->setNumber($player['number']);
            $p->setLeader($player['leader']);
            $p->setWeight($player['weight']);
            $em->persist($p);
        }

        $em->flush();

        echo " ---------------------------------\n";
        echo " > " . $playerNum . " players added.\n";
        echo " > " . $playerNumU . " players updated.\n";
        echo " ---------------------------------\n";

        $playerObjects = $em->getRepository('AppBundle:Player')->findAll();
        $players = array();
        foreach ($playerObjects as $playerObject) {
            $players['p' . $playerObject->getNumber()] = $playerObject;
        }

        foreach ($teams as $team) {
            $t = $em->getRepository('AppBundle:Team')->findOneBy(array('name' => $team['name']));
            if ($t) {
                $teamNumU++;
            }
            else {
                $t = new Team();
                $teamNum++;
            }

            /** @var Player $p1 */
            $p1 = $players['p' . $team['p1']];
            /** @var Player $p2 */
            $p2 = $players['p' . $team['p2']];
            /** @var Player $p3 */
            $p3 = $players['p' . $team['p3']];

            $t->setName($team['name']);
            $t->setPoint($team['point']);
            $em->persist($t);

            $p1->setTeam($t);
            $p2->setTeam($t);
            $p3->setTeam($t);
            $em->persist($p1);
            $em->persist($p2);
            $em->persist($p3);
        }

        $em->flush();

        echo " > " . $teamNum . " teams added.\n";
        echo " > " . $teamNumU . " teams updated.\n";
        echo " ---------------------------------\n";

        foreach ($users as $user) {
            $u = $em->getRepository('AppBundle:GoogleUser')->findOneBy(array('username' => $user['name']));
            if ($u) {
                $userNumU++;
            }
            else {
                $u = new GoogleUser();
                $userNum++;
            }

            $u->setUserName($user['name']);
            $u->setFullName($user['fullName']);
            $u->setEmail($user['mail']);
            $u->setRule($user['rule']);
            $u->setPassword('1f5bf9b04e093cf622d20c3454075700');
            $em->persist($u);
        }

        $em->flush();

        echo " > " . $userNum . " users added.\n";
        echo " > " . $userNumU . " users updated.\n";
        echo " ---------------------------------\n";

        foreach ($gameTypes as $gameType) {
            $m = $em->getRepository('AppBundle:GameType')->findOneBy(array('name' => $gameType['name']));
            if ($m) {
                $gameTypeNumU++;
            }
            else {
                $m = new GameType();
                $gameTypeNum++;
            }

            $m->setName($gameType['name']);
            $m->setComputerName($gameType['computerName']);
            $m->setWeight($gameType['weight']);

            $em->persist($m);
        }

        $em->flush();

        echo " > " . $gameTypeNum . " game types added.\n";
        echo " > " . $gameTypeNumU . " game types updated.\n";
        echo " ---------------------------------\n";

        foreach ($kerchiefTypes as $kerchiefType) {
            $k = $em->getRepository('AppBundle:KerchiefType')->findOneBy(array('computerName' => $kerchiefType['computerName']));
            if ($k) {
                $kerchiefTypeNumU++;
            }
            else {
                $k = new KerchiefType();
                $kerchiefTypeNum++;
            }

            $k->setName($kerchiefType['name']);
            $k->setComputerName($kerchiefType['computerName']);
            $k->setWeight($kerchiefType['weight']);

            $em->persist($k);
        }

        $em->flush();

        echo " > " . $kerchiefTypeNum . " kerchief types added.\n";
        echo " > " . $kerchiefTypeNumU . " kerchief types updated.\n";
        echo " ---------------------------------\n";

        foreach ($roundResultTypes as $roundResultType) {
            $rrt = $em->getRepository('AppBundle:RoundResultType')->findOneBy(array('name' => $roundResultType['name']));
            if ($rrt) {
                $roundResultTypeNumU++;
            }
            else {
                $rrt = new RoundResultType();
                $roundResultTypeNum++;
            }

            $rrt->setName($roundResultType['name']);
            $rrt->setComputerName($roundResultType['computerName']);
            $rrt->setPoint($roundResultType['point']);

            $em->persist($rrt);
        }

        $em->flush();

        echo " > " . $roundResultTypeNum . " round result types added.\n";
        echo " > " . $roundResultTypeNumU . " round result types updated.\n";
        echo " ---------------------------------\n";

        for ($i = 1; $i <= 10; $i++) {
            $r = $em->getRepository('AppBundle:Round')->findOneBy(array('number' => $i));
            if ($r) {
                $roundNumU++;
            }
            else {
                $r = new Round();
                $roundNum++;
            }

            $r->setNumber($i);
            $r->setName($i . '. forduló (2016-xx-xx)');

            $em->persist($r);

            $gameTypes = $em->getRepository('AppBundle:GameType')->findAll();
            foreach ($gameTypes as $gameType) {
                $game = $em->getRepository('AppBundle:Game')->findOneBy(array(
                    'round' => $r,
                    'gameType' => $gameType,
                ));
                if (!$game) {
                    $game = new Game();
                    $game->setRound($r);
                    $game->setGameType($gameType);

                    $em->persist($game);
                    $gameNum++;
                }
            }
        }

        $em->flush();

        echo " > " . $roundNum . " rounds added.\n";
        echo " > " . $roundNumU . " rounds updated.\n";
        echo " > " . $gameNum . " games added.\n";
        echo " ---------------------------------\n";

        $s = $em->getRepository('AppBundle:Settings')->findOneBy(array('name' => 'currentRound'));
        if ($s) {
            echo " > 1 setting updated.\n";
        }
        else {
            $s = new Settings();
            echo " > 1 setting added.\n";
        }

        $s->setName('currentRound');
        $s->setValue(1);

        $em->persist($s);
        $em->flush();

        echo " ---------------------------------\n";
    }

    protected function getData() {
        $players = array(
            array(
                'name'   => 'Gaur',
                'number' => 0,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Kuszkusz',
                'number' => 1,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Bika',
                'number' => 3,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Pávi',
                'number' => 4,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Jak',
                'number' => 5,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Sas',
                'number' => 6,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Csiga',
                'number' => 7,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Gyík',
                'number' => 8,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Seri',
                'number' => 10,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Orka',
                'number' => 12,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Hero',
                'number' => 13,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Tücsök',
                'number' => 14,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Bloby',
                'number' => 15,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Vé',
                'number' => 16,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Sikló',
                'number' => 21,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'MPeti',
                'number' => 23,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Kengu',
                'number' => 27,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Szarvas',
                'number' => 29,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Vércse',
                'number' => 32,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Alfa',
                'number' => 33,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Bolha',
                'number' => 42,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Zebi',
                'number' => 44,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Csacsi',
                'number' => 46,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Légy',
                'number' => 56,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Csöpi',
                'number' => 63,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Gergő',
                'number' => 69,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'Crow',
                'number' => 76,
                'leader' => false,
                'weight' => 2,
            ),
            array(
                'name'   => 'Kiang',
                'number' => 83,
                'leader' => false,
                'weight' => 3,
            ),
            array(
                'name'   => 'FekaMaci',
                'number' => 91,
                'leader' => true,
                'weight' => 1,
            ),
            array(
                'name'   => 'Coyote',
                'number' => 98,
                'leader' => true,
                'weight' => 1,
            ),
        );

        $teams = array(
            array(
                'name'  => 'Dübelz',
                'p1'    => 46,
                'p2'    => 76,
                'p3'    => 83,
                'point' => 0,
            ),
            array(
                'name'  => 'Vikings',
                'p1'    => 44,
                'p2'    => 0,
                'p3'    => 69,
                'point' => 0,
            ),
            array(
                'name'  => 'Muskétások',
                'p1'    => 14,
                'p2'    => 29,
                'p3'    => 63,
                'point' => 0,
            ),
            array(
                'name'  => 'Wild Giants',
                'p1'    => 8,
                'p2'    => 12,
                'p3'    => 23,
                'point' => 0,
            ),
            array(
                'name'  => 'Hunters',
                'p1'    => 27,
                'p2'    => 56,
                'p3'    => 1,
                'point' => 0,
            ),
            array(
                'name'  => 'Regnum',
                'p1'    => 5,
                'p2'    => 42,
                'p3'    => 16,
                'point' => 0,
            ),
            array(
                'name'  => 'Meteors',
                'p1'    => 6,
                'p2'    => 13,
                'p3'    => 4,
                'point' => 0,
            ),
            array(
                'name'  => 'Playful Children',
                'p1'    => 3,
                'p2'    => 15,
                'p3'    => 32,
                'point' => 0,
            ),
            array(
                'name'  => 'Myrmidons',
                'p1'    => 98,
                'p2'    => 10,
                'p3'    => 21,
                'point' => 0,
            ),
            array(
                'name'  => 'Dragons',
                'p1'    => 91,
                'p2'    => 7,
                'p3'    => 33,
                'point' => 0,
            ),
        );

        $users = array(
            array(
                'name' => 'BúsGergő',
                'fullName' => 'Bús Gergely',
                'mail' => 'gergely.bus@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'HGergő',
                'fullName' => 'Horváth Gergely',
                'mail' => 'gregoriosz83@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Csöpi',
                'fullName' => 'Csépány Gergő',
                'mail' => 'cheoppy@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Klaudia',
                'fullName' => 'Horváth Klaudia',
                'mail' => 'horvathklau@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'István',
                'fullName' => 'Szász István',
                'mail' => 'szaszistvan78@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Viki',
                'fullName' => 'Cseh Viktória',
                'mail' => 'lessza@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'MPeti',
                'fullName' => 'Majoros Péter',
                'mail' => 'mpeet789@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Gergő',
                'fullName' => 'Nagy Gergő',
                'mail' => 'gergo.akademia@gmail.com',
                'rule' => 3,
            ),
            array(
                'name' => 'Robi',
                'fullName' => 'Kosztovics Róbert',
                'mail' => 'robert.kosztovics@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Alfa',
                'fullName' => 'Szomolányi Tamás',
                'mail' => 'tamas.szomolanyi@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Ajakosmaci',
                'fullName' => 'Varga Betti',
                'mail' => 'vabetty@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Ali',
                'fullName' => 'Simon Zsombor',
                'mail' => 'simon.zsombor@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Antilop',
                'fullName' => 'Szendrei Ágnes',
                'mail' => 'antilop.totem@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Bika',
                'fullName' => 'Nagy Viktor',
                'mail' => 'akasha.bika@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Bloby',
                'fullName' => 'Kovács Bálint',
                'mail' => 'balint.tanul@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Bolha',
                'fullName' => 'Zakariás László',
                'mail' => 'tsg.pictures@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Coyote',
                'fullName' => 'Török-Szabó Balázs',
                'mail' => 'wileecoyote098@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Crow',
                'fullName' => 'Sás Zoltán',
                'mail' => 'carakro.hollo@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Csiga',
                'fullName' => 'Szelecsényi Gábor',
                'mail' => 'boncmester@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Csikóhal',
                'fullName' => 'Varga Fruzsina',
                'mail' => 'vfruska@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Darázs',
                'fullName' => 'Gályász Judit',
                'mail' => 'darazskiralyno@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Delfin',
                'fullName' => 'Csovcsics Kitti',
                'mail' => 'csovcsicskitti@gmail.com',
                'rule' => 1,
            ),
            array(
                'name' => 'Dikdik',
                'fullName' => 'Sípos Zsófia',
                'mail' => 'kispatas.dikdik@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Emu',
                'fullName' => 'Kürti Nóra',
                'mail' => 'nora.kurti@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Farkas',
                'fullName' => 'Heredi Renáta',
                'mail' => 'heredi.renata@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Feka',
                'fullName' => 'Kovács Zsombor',
                'mail' => 'zsombor.kovacs@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Fóka',
                'fullName' => 'László Szandra',
                'mail' => 'laszlo.szandra@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Gazella',
                'fullName' => 'Barna Nikoletta',
                'mail' => 'barkaniki@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Gibbon',
                'fullName' => 'Nyőgériné Majoros Tímea',
                'mail' => 'gibbi0629@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Gópi',
                'fullName' => 'Odrovits Dóra',
                'mail' => 'gopi.hiuz@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Gyík',
                'fullName' => 'Hacker Péter',
                'mail' => 'gyikszkukac@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Hero',
                'fullName' => 'Bosnyák Zoltán',
                'mail' => 'bosnyakz@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Jak',
                'fullName' => 'Gyuris Zsolt',
                'mail' => 'legendaryzsolt@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Katacsőr',
                'fullName' => 'Marosi Katalin',
                'mail' => 'marosikatalin5@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Kanári',
                'fullName' => 'Pintér Blanka',
                'mail' => 'pinterblanka79@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Kecsi',
                'fullName' => 'Bajzik Éva',
                'mail' => 'karya.kecske@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Kengu',
                'fullName' => 'Bohus Gergely',
                'mail' => 'bohusgergely@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Kiang',
                'fullName' => 'Szalai Szabolcs',
                'mail' => 'szabolcsszalai17@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Kuszkusz',
                'fullName' => 'Benedek Feri',
                'mail' => 'bferenc79@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Kutya',
                'fullName' => 'Mazán Bea',
                'mail' => 'bea.mazan@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Yayi',
                'fullName' => 'Gáspár Emese',
                'mail' => 'ayi.arm@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Laksh',
                'fullName' => 'Angyal Szilvia',
                'mail' => 'angyalsz@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Légy',
                'fullName' => 'Szabó András',
                'mail' => 'yericho@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Lepke',
                'fullName' => 'Klement Ágnes',
                'mail' => 'klementagi@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Medúza',
                'fullName' => 'Horogh Petra',
                'mail' => 'phorogh@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Méhecske',
                'fullName' => 'Kiss Karolina',
                'mail' => 'mehecske04@gmail.com',
                'rule' => 1,
            ),
            array(
                'name' => 'Mókus',
                'fullName' => 'Kertész Renáta',
                'mail' => 'kerrenata@gmail.com',
                'rule' => 1,
            ),
            array(
                'name' => 'Mongúz',
                'fullName' => 'Nagy Melinda',
                'mail' => 'rozsda15@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Mormesz',
                'fullName' => 'Kincses Adrienn',
                'mail' => 'kin.adri@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Nandu',
                'fullName' => 'Dudás Ági',
                'mail' => 'dudas.agnes@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Nyérc',
                'fullName' => 'Szabó Gabriella',
                'mail' => 'szagobago@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Nyuszi',
                'fullName' => 'Körmendi Nóra',
                'mail' => 'ny1931@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Orca',
                'fullName' => 'Vető Miklós',
                'mail' => 'vetomik@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Paci',
                'fullName' => 'Gáspár Dóra',
                'mail' => 'paci.kamli@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Panda',
                'fullName' => 'Harsányi Judit',
                'mail' => 'harsanyi.judit@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Parvati',
                'fullName' => 'Vallyon Nikoletta',
                'mail' => 'parvativati@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Pávián',
                'fullName' => 'Eizenberger Máté',
                'mail' => 'ematthew6@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Pele',
                'fullName' => 'Petró Éva',
                'mail' => 'kertipele123@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Pocok',
                'fullName' => 'Hajdu Éva',
                'mail' => 'hajdueva.82@gmail.com',
                'rule' => 1,
            ),
            array(
                'name' => 'Róka',
                'fullName' => 'Augusztin Olga',
                'mail' => 'olga.augusztin@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Sas',
                'fullName' => 'Kovács Zsolt',
                'mail' => 'asasleszallt@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Seri',
                'fullName' => 'Kovács Tamás',
                'mail' => 'kovacsson@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Sikló',
                'fullName' => 'Bézi Balázs',
                'mail' => 'bezibalazs@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Szamár',
                'fullName' => 'Nacsa Gergely',
                'mail' => 'gergely.nacsa@googlemail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Szarvas',
                'fullName' => 'Nemcsák László',
                'mail' => 'szarvas.totem@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Szita',
                'fullName' => 'Koncz Zsuzsanna',
                'mail' => 'csillag19426@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Szöcsi',
                'fullName' => 'Dudás Anna',
                'mail' => 'dr.dudas.anna@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Szunyi',
                'fullName' => 'Kondráth Teodóra',
                'mail' => 'kondrath.teo@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Tenge',
                'fullName' => 'Gajdács Réka',
                'mail' => 'tengelic8@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'TM',
                'fullName' => 'Illéssy Kata',
                'mail' => 'tm.cavia@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Tücsi',
                'fullName' => 'Blaskovics Viktor',
                'mail' => 'mezeitucsok@gmail.com',
                'rule' => 2,
            ),
            array(
                'name' => 'Vé',
                'fullName' => 'Sándor Csaba',
                'mail' => 'bagolyberti@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Vércse',
                'fullName' => 'Nyőgéri Zsolt',
                'mail' => 'yolti.hu@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Viziőz',
                'fullName' => 'Bisz Emőke',
                'mail' => 'biszemo@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Zebra',
                'fullName' => 'Tóth Tamás',
                'mail' => 'zebratotem@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Domi',
                'fullName' => 'Szabadi Dominik',
                'mail' => 'szabadi.dominik@gmail.com',
                'rule' => 0,
            ),
            array(
                'name' => 'Rebi',
                'fullName' => 'Antal Rebeka',
                'mail' => 'rebyxd@gmail.com',
                'rule' => 0,
            ),
        );

        $gameTypes = array(
            array(
                'name' => 'Előselejtező #1',
                'computerName' => 'eloselejtezo_1',
                'weight' => 1,
            ),
            array(
                'name' => 'Előselejtező #2',
                'computerName' => 'eloselejtezo_2',
                'weight' => 2,
            ),
            array(
                'name' => '"A" csoport körmérkőzés #1',
                'computerName' => 'a_csoport_kormerkozes_1',
                'weight' => 3,
            ),
            array(
                'name' => '"A" csoport körmérkőzés #2',
                'computerName' => 'a_csoport_kormerkozes_2',
                'weight' => 4,
            ),
            array(
                'name' => '"A" csoport körmérkőzés #3',
                'computerName' => 'a_csoport_kormerkozes_3',
                'weight' => 5,
            ),
            array(
                'name' => '"A" csoport körmérkőzés #4',
                'computerName' => 'a_csoport_kormerkozes_4',
                'weight' => 6,
            ),
            array(
                'name' => '"A" csoport körmérkőzés #5',
                'computerName' => 'a_csoport_kormerkozes_5',
                'weight' => 7,
            ),
            array(
                'name' => '"A" csoport körmérkőzés #6',
                'computerName' => 'a_csoport_kormerkozes_6',
                'weight' => 8,
            ),
            array(
                'name' => '"B" csoport körmérkőzés #1',
                'computerName' => 'b_csoport_kormerkozes_1',
                'weight' => 9,
            ),
            array(
                'name' => '"B" csoport körmérkőzés #2',
                'computerName' => 'b_csoport_kormerkozes_2',
                'weight' => 10,
            ),
            array(
                'name' => '"B" csoport körmérkőzés #3',
                'computerName' => 'b_csoport_kormerkozes_3',
                'weight' => 11,
            ),
            array(
                'name' => '"B" csoport körmérkőzés #4',
                'computerName' => 'b_csoport_kormerkozes_4',
                'weight' => 12,
            ),
            array(
                'name' => '"B" csoport körmérkőzés #5',
                'computerName' => 'b_csoport_kormerkozes_5',
                'weight' => 13,
            ),
            array(
                'name' => '"B" csoport körmérkőzés #6',
                'computerName' => 'b_csoport_kormerkozes_6',
                'weight' => 14,
            ),
            array(
                'name' => 'Helyosztó a 7-8. helyért',
                'computerName' => 'helyoszto_7_8',
                'weight' => 15,
            ),
            array(
                'name' => 'Helyosztó az 5-6. helyért',
                'computerName' => 'helyoszto_5_6',
                'weight' => 16,
            ),
            array(
                'name' => 'Elődöntő #1',
                'computerName' => 'elodonto_1',
                'weight' => 17,
            ),
            array(
                'name' => 'Elődöntő #2',
                'computerName' => 'elodonto_2',
                'weight' => 18,
            ),
            array(
                'name' => 'Mérkőzés a 3. helyért',
                'computerName' => 'merkozes_3',
                'weight' => 19,
            ),
            array(
                'name' => 'Döntő',
                'computerName' => 'donto',
                'weight' => 20,
            ),
        );

        $kerchiefTypes = array(
            array(
                'name' => 'Gól',
                'computerName' => 'G',
                'weight' => 1,
            ),
            array(
                'name' => '2 pontos /sniper/',
                'computerName' => 'S',
                'weight' => 2,
            ),
            array(
                'name' => '1 pontos /akció/',
                'computerName' => 'A',
                'weight' => 3,
            ),
            array(
                'name' => 'Kanadai',
                'computerName' => 'P',
                'weight' => 4,
            ),
            array(
                'name' => 'Kapusbravúr',
                'computerName' => 'GK',
                'weight' => 5,
            ),
            array(
                'name' => 'Kapus labdaszerzés',
                'computerName' => 'GKI',
                'weight' => 6,
            ),
            array(
                'name' => 'Gólpassz',
                'computerName' => 'GA',
                'weight' => 7,
            ),
            array(
                'name' => 'Blokk',
                'computerName' => 'D',
                'weight' => 8,
            ),
            array(
                'name' => 'Labdaszerzés',
                'computerName' => 'I',
                'weight' => 9,
            ),
            array(
                'name' => 'Büntető',
                'computerName' => 'PE',
                'weight' => 10,
            ),
        );

        $roundResultTypes = array(
            array(
                'name' => '1. hely',
                'computerName' => 'hely_1',
                'point' => 12,
            ),
            array(
                'name' => '2. hely',
                'computerName' => 'hely_2',
                'point' => 9,
            ),
            array(
                'name' => '3. hely',
                'computerName' => 'hely_3',
                'point' => 7,
            ),
            array(
                'name' => '4. hely',
                'computerName' => 'hely_4',
                'point' => 5,
            ),
            array(
                'name' => '5. hely',
                'computerName' => 'hely_5',
                'point' => 4,
            ),
            array(
                'name' => '6. hely',
                'computerName' => 'hely_6',
                'point' => 3,
            ),
            array(
                'name' => '7. hely',
                'computerName' => 'hely_7',
                'point' => 2,
            ),
            array(
                'name' => '8. hely',
                'computerName' => 'hely_8',
                'point' => 1,
            ),
            array(
                'name' => '9. hely',
                'computerName' => 'hely_9',
                'point' => 0,
            ),
            array(
                'name' => '10. hely',
                'computerName' => 'hely_10',
                'point' => 0,
            ),
        );

        return array(
            'players'          => $players,
            'teams'            => $teams,
            'users'            => $users,
            'gameTypes'        => $gameTypes,
            'kerchiefTypes'    => $kerchiefTypes,
            'roundResultTypes' => $roundResultTypes,
        );
    }
}