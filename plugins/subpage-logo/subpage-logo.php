<?php
/*
Plugin Name: Subpage logo
Description: Extract the logo's filename from path, and show the image
*/
/* Start Adding Functions Below this Line */

// Creating the widget
class wpb_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            // Base ID of your widget
            'wpb_widget',

            // Widget name will appear in UI
            __('Subpage logo', 'wpb_widget_domain'),

            // Widget description
            array( 'description' => __( 'Shows the logo as extracted from the page path', 'wpb_widget_domain' ), )
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
        $basepath = plugins_url('images/', __FILE__);
        $main = get_post();
        while($main->post_parent) {
            $main = get_post($main->post_parent);
        }

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        ?>
        <img src="<?php echo $basepath . $main->post_name . "_logo.png"; ?>" />
        <?php

        echo $args['after_widget'];
    }

    // Widget Backend
    public function form( $instance ) {

    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        return $instance;
    }
} // Class wpb_widget ends here


// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );